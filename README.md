## Deploy the app

#### Login at CF
```
cf login
```

#### Create HDI Container
```
cf create-service hana hdi-shared bookshop-db-hdi-container
```
On trial account use hanatrial instead of hana

#### Build the application and push to cf
```
cds build/all
cf push -f gen/db
cf push -f gen/srv --random-route
```
Grab the URL of the service application and insert into the destination/url in app/default-env.json

#### Generate xs-security.json
```
cds compile srv/ --to xsuaa > gen/xs-security.json
```

#### Create XSUAA service
```
cf create-service xsuaa application bookshop-uaa -c gen/xs-security.json
```

Note: To update the service use: `cf update-service bookshop-uaa -c gen/xs-security.json`

#### Create the service key

```
cf create-service-key bookshop-uaa bookshop-uaa-key
cf service-key bookshop-uaa bookshop-uaa-key
```

Copy the snippet into the app/default-env.json.sample in the credentials block and rename it to default-env.json. Do the same for default-env.json.sample in the root directory.

Now we can deploy the approuter to CF
```
cd app
cf push bookshop --no-start --no-manifest --random-route
cf bind-service bookshop bookshop-uaa   
cf start bookshop
```
